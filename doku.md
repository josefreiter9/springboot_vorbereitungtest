## Auswahl der Dependencies

* Lombok
* Spring Web
* Spring Data JPA
* H2 Database

![IntelliJ](screenshots/Screenshot_01.png)

## application.yml

Einstellen von Server-Port, H2-Database und JPA-Plattform

![IntelliJ](screenshots/Screenshot_02.png)

## Entität - Book erstellen

![IntelliJ](screenshots/Screenshot_03.png)

## Repository - Interface: BookRepository erstellen

Wichtig:
* Interface
* @Repository
* extends JpaRepository<Book, Long>

![IntelliJ](screenshots/Screenshot_04.png)

## Service - Interface: BookService erstellen

Wichtig:
* Interface
* Alle notwendigen Methoden-Köpfe für die jeweilige Entität auflisten.

![IntelliJ](screenshots/Screenshot_05.png)

## Service - Implementierung - Klasse: BookServiceImpl erstellen

Wichtig:
* @Service
* implements BookService
* Datenfeld 'bookRepository' verwenden
* Hier werden alle Methoden vom Interface - BookService ausimplementiert

![IntelliJ](screenshots/Screenshot_06.png)

## RestController-Klasse: BookController erstellen

Wichtig:
* @RestController
* Datenfeld 'bookService' verwenden
* Hier werden alle Rest-Methoden aufgelistet

![IntelliJ](screenshots/Screenshot_07.png)

## Erstelle alle notwendigen Methoden im Interface BookService

Zuerst werden alle Methoden im Interface aufgeschrieben, um diese später im RestController zu verwenden.
Anschießend werden die Methoden in der Klasse BookServiceImpl ausimplementiert.

![IntelliJ](screenshots/Screenshot_08.png)

## Erstelle alle Rest-Methoden im BookController

Es werden alle notwendigen Rest-Methoden im BookController erstellt.

Wichtig:
* @RestController

![IntelliJ](screenshots/Screenshot_09.png)

## Ausimplementieren aller Methoden aus Interface - BookService in BookServiceImpl

Wichtig:
* @Service
* Datenfeld bookRepository um CRUD-Methoden aus JpaRepository zu verwenden
* Implementieren / überschreiben aller Methoden 
* Jede Methode einzeln ausimplementieren

![IntelliJ](screenshots/Screenshot_77.png)

## Erstellen der Exception: BookNotFoundException, NoBooksFoundException, BookAttributesNotValidException

Im Package 'exception' werden die beiden Exception-Klasse abgelegt.

Wichtig:
* erben von Exception - extends Exception
* weiterleiten des String message-Parameters and Konstuktor der Superklasse -> super(message);

## Klasse HttpResponse erstellen, welche als Vorlage für ExceptionController dient

Man benötigt ein Datenfeld mit dem Status und ein Datenfeld mit einer Response-Nachricht.

![IntelliJ](screenshots/Screenshot_11.png)

## ExceptionController erstellen

Im ExceptionController werden alle Exceptions aufgelistet

Wichtig:
* @ControllerAdvice
* @ResponseStatus

![IntelliJ](screenshots/Screenshot_12.png)

## H2-Datenbank - RestController-Methoden testen

Es müssen folgende Werte eingestellt werden:

* JDBC URL -> Url aus application.yml (Url auf der die H2-Datenbank gehostet wird)
* User Name -> Benutzername aus application.yml
* Password -> Passwort aus application.yml

![IntelliJ](screenshots/Screenshot_99.png)