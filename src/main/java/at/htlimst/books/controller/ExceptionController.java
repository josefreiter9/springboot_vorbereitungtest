package at.htlimst.books.controller;

import at.htlimst.books.entity.HttpResponse;
import at.htlimst.books.exceptions.BookAttributesNotValidException;
import at.htlimst.books.exceptions.BookNotFoundException;
import at.htlimst.books.exceptions.NoBooksFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
@ResponseStatus
public class ExceptionController extends ResponseEntityExceptionHandler {

    //Create an HTTP-Response for BookNotFoundException
    @ExceptionHandler(BookNotFoundException.class)
    public ResponseEntity<HttpResponse> bookNotFoundException(BookNotFoundException exception, WebRequest webRequest){
        HttpResponse message = new HttpResponse(HttpStatus.NOT_FOUND, exception.getMessage());  //create HttpResponse-Object

        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(message);   //return HTTP-Response with status and with message as body
    }

    //Create an HTTP-Response for NoBooksFoundException
    @ExceptionHandler(NoBooksFoundException.class)
    public ResponseEntity<HttpResponse> noBooksFoundException(NoBooksFoundException exception, WebRequest webRequest){
        HttpResponse message = new HttpResponse(HttpStatus.NOT_FOUND, exception.getMessage());  //create HttpResponse-Object

        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(message);   //return HTTP-Response with status and with message as body
    }

    //Create an HTTP-Response for BookAttributesNotValidException
    @ExceptionHandler(BookAttributesNotValidException.class)
    public ResponseEntity<HttpResponse> bookAttributesNotValidException(BookAttributesNotValidException exception, WebRequest webRequest){
        HttpResponse message = new HttpResponse(HttpStatus.BAD_REQUEST, exception.getMessage());

        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(message);
    }
}
