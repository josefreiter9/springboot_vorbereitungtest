package at.htlimst.books.controller;

import at.htlimst.books.entity.Book;
import at.htlimst.books.exceptions.BookAttributesNotValidException;
import at.htlimst.books.exceptions.BookNotFoundException;
import at.htlimst.books.exceptions.NoBooksFoundException;
import at.htlimst.books.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * This class is responsible for managing all HTTP-Requests
 */
@RestController
public class BookController {

    private static final String RESPONSE_HEADER_PREF = "127.0.0.1:8080/";  //create HTTP-response header prefix

    @Autowired
    private BookService bookService;    //create field 'bookService' to access to bookService-Methods

    @PostMapping("/saveBook")
    public ResponseEntity<Book> saveBook(@RequestBody Book book) throws BookAttributesNotValidException {
        Book saveBook = bookService.saveBook(book);

        HttpHeaders responseHeader = new HttpHeaders(); //initialize HttpHeaders-Object to set different HTTP-Headers
        responseHeader.set("Location", RESPONSE_HEADER_PREF+"getBook/" + saveBook.getId()); //set an HTTP-Header field for Location-entry

        return ResponseEntity.status(HttpStatus.OK).headers(responseHeader).body(saveBook);  //Response with ResponseEntity<Book> -> status, header, body
    }

    @GetMapping("/getBook/{id}")
    public ResponseEntity<Book> getBookById(@PathVariable("id") Long bookId) throws BookNotFoundException {
        Book bookResponse = bookService.getBookById(bookId);

        HttpHeaders responseHeader = new HttpHeaders();
        responseHeader.set("Location", RESPONSE_HEADER_PREF+"getBook/" + bookResponse.getId());

        return ResponseEntity.status(HttpStatus.OK).headers(responseHeader).body(bookResponse);
    }

    @GetMapping("/getAllBooks")
    public ResponseEntity<List<Book>> getAllBooks() throws NoBooksFoundException {
        List<Book> allBooksResponse = bookService.getAllBooks();

        HttpHeaders responseHeader = new HttpHeaders();
        responseHeader.set("Location", RESPONSE_HEADER_PREF+"getAllBooks");


        return ResponseEntity.status(HttpStatus.OK).headers(responseHeader).body(allBooksResponse);
    }

    @PutMapping("/updateBook/{id}")
    public ResponseEntity<Book> updateBookById(@PathVariable("id") Long bookId, @RequestBody Book book) throws BookNotFoundException {
        Book bookResponse = bookService.updateBookById(bookId, book);

        HttpHeaders responseHeader = new HttpHeaders(); //initialize HttpHeaders-Object to set different HTTP-Headers
        responseHeader.set("Location", RESPONSE_HEADER_PREF+"getBook/" + bookResponse.getId()); //set an HTTP-Header field for Location-entry

        return ResponseEntity.status(HttpStatus.OK).headers(responseHeader).body(bookResponse);
    }

    @DeleteMapping("/deleteBook/{id}")
    public ResponseEntity<String>  deleteBookById(@PathVariable("id") Long bookId) throws BookNotFoundException {
        String response = bookService.deleteBookById(bookId);

        return ResponseEntity.status(HttpStatus.OK).body(response); //response with a ResponseEntity<String> when book was deleted
    }
}
