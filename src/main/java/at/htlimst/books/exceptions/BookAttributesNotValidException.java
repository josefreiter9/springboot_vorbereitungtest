package at.htlimst.books.exceptions;

public class BookAttributesNotValidException extends Exception {
    public BookAttributesNotValidException(String message) {
        super(message);
    }
}
