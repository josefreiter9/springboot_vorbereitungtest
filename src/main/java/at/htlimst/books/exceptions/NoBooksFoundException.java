package at.htlimst.books.exceptions;

public class NoBooksFoundException extends Exception {
    public NoBooksFoundException(String message) {
        super(message);
    }
}
