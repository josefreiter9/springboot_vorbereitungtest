package at.htlimst.books.service;

import at.htlimst.books.entity.Book;
import at.htlimst.books.exceptions.BookAttributesNotValidException;
import at.htlimst.books.exceptions.BookNotFoundException;
import at.htlimst.books.exceptions.NoBooksFoundException;
import at.htlimst.books.repository.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class BookServiceImpl implements BookService{

    @Autowired
    private BookRepository bookRepository;  //create field 'bookRepository' to access to JPA-CRUD-Methods

    @Override
    public Book saveBook(Book book) throws BookAttributesNotValidException {
        if(book.getCategory().isEmpty() || book.getIsbn() > Integer.MAX_VALUE || book.getIsbn() < Integer.MIN_VALUE || book.getTitle().isEmpty()){  //check if all Book attributes are valid
            throw new BookAttributesNotValidException("Book Attributes not valid!");
        }
        Book savedBook = bookRepository.save(book);
        return savedBook;
    }

    @Override
    public Book getBookById(Long id) throws BookNotFoundException {
        Optional<Book> book = bookRepository.findById(id);

        if(!book.isPresent()){
            throw new BookNotFoundException("Book not found with id: "+id);
        }
        return book.get();
    }

    @Override
    public List<Book> getAllBooks() throws NoBooksFoundException {
        List<Book> allBooks = bookRepository.findAll();

        if(allBooks.isEmpty()){
            throw new NoBooksFoundException("No books found!");
        }
        return allBooks;
    }

    @Override
    public Book updateBookById(Long id, Book book) throws BookNotFoundException {
        Optional<Book> optionalBook = bookRepository.findById(id);

        if(!optionalBook.isPresent()){
            throw new BookNotFoundException("Book not found with id: "+id);
        }

        Book bookToUpdate = optionalBook.get();  //create new bookToUpdate entity

        bookToUpdate.setTitle(book.getTitle());  //assign new attributes to bookToUpdate-entity
        bookToUpdate.setCategory(book.getCategory());
        bookToUpdate.setIsbn(book.getIsbn());

        Book updatedBook = bookRepository.save(bookToUpdate);   //save updatedBook to database
        return updatedBook;
    }

    @Override
    public String deleteBookById(Long id) throws BookNotFoundException {
        Optional<Book> bookToDeleteOpt = bookRepository.findById(id);

        if(!bookToDeleteOpt.isPresent()){
            throw new BookNotFoundException("Book not found with id: "+id);
        }
        bookRepository.delete(bookToDeleteOpt.get());

        return "Book with id: "+id+" deleted.";
    }

    //Override all Methods from Interface - BookService

    //Use CRUD-Methods from Interface BookRepository
}
