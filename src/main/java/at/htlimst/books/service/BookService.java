package at.htlimst.books.service;

import at.htlimst.books.entity.Book;
import at.htlimst.books.exceptions.BookAttributesNotValidException;
import at.htlimst.books.exceptions.BookNotFoundException;
import at.htlimst.books.exceptions.NoBooksFoundException;

import java.util.List;

public interface BookService {

    //List all Methods-Headers which are necessary for this project

    Book saveBook(Book book) throws BookAttributesNotValidException;   //save a book-entity
    Book getBookById(Long id) throws BookNotFoundException;  //get a book from id
    List<Book> getAllBooks() throws NoBooksFoundException;   //get all books in a list
    Book updateBookById(Long bookId, Book book) throws BookNotFoundException;   //update book-entity
    String deleteBookById(Long id) throws BookNotFoundException;   //delete a book-entity
}
